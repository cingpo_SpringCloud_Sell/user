FROM hub.c.163.com/library/java:8-alpine

MAINTAINER Peter.Chen cingpo@sina.com

ADD user-server/target/*.war app.war

EXPOSE 8007

ENTRYPOINT ["java", "-jar", "/app.war"]

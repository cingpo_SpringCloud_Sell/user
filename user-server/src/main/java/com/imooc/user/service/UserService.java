package com.imooc.user.service;

import com.imooc.user.dataobject.UserInfo;

/**
 * @author Peter, Chen
 * @date 2019-01-02 18:20
 */
public interface UserService {

    /**
     * 通过openid来查询用户信息
     * @param openid
     * @return
     */
    UserInfo findByOpenid(String openid);
}

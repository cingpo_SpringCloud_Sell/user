package com.imooc.user.repository;

import com.imooc.user.dataobject.UserInfo;
import org.apache.catalina.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Peter, Chen
 * @date 2019-01-02 18:18
 */
public interface UserInfoRepository extends JpaRepository<UserInfo, String> {

    UserInfo findByOpenid(String openid);
}

package com.imooc.user.VO;

import lombok.Data;

/**
 * @author Peter, Chen
 * @date 2018/11/16 3:46 PM
 */
@Data
public class ResultVO<T> {
    private Integer code;
    private String msg;
    private T data;
}

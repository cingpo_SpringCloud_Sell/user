package com.imooc.user.enums;

import lombok.Getter;

/**
 * @author Peter, Chen
 * @date 2019-01-03 16:02
 */
@Getter
public enum RoleEnum {
    BUYER(1, "买家"),
    SELLER(2, "卖家"),
    ;

    private Integer code;
    private String messae;

    public Integer getCode() {
        return code;
    }

    RoleEnum(Integer code, String messae) {
        this.code = code;
        this.messae = messae;
    }}

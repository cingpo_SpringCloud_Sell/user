package com.imooc.user.enums;

import lombok.Getter;

/**
 * @author Peter, Chen
 * @date 2018/11/19 8:56 PM
 */
@Getter
public enum ResultEnum {
    LOGIN_FAILED(1, "登录失败"),
    ROLE_ERROR(2, "角色权限有误")
    ;

    private Integer code;
    private String message;

    ResultEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}

package com.imooc.user.constant;

/**
 * @author Peter, Chen
 * @date 2019-01-04 18:23
 */
public interface CookieConstant {

    String TOKEN = "token";

    String OPENID = "openid";

    /**
     * 过期时间（单位：s)
     */
    Integer expire = 7200;
}

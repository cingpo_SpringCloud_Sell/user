package com.imooc.user.constant;

/**
 * @author Peter, Chen
 * @date 2019-01-14 16:16
 */
public interface RedisConstant {

    String TOKEN_TEMPLATE = "token_%s";
}
